## Current Projects / Initiatives

### Testing

- #### Code Testing and Coverage Category Maturity Research
    - **Issue:** https://gitlab.com/gitlab-org/ux-research/-/issues/1034
    - **Issue(2):** https://gitlab.com/gitlab-org/ux-research/-/issues/1070
    - **Description:** We are working a Category Maturity Scorecard for Code Testing and Code Coverage with the idea of moving these categories to their next maturity level.
    - **Status:** Currently waiting for JTBD validation survey.

<hr>

- #### Display the current coverage data for each selected project
    - **Issue:** https://gitlab.com/gitlab-org/gitlab/-/issues/215135
    - **Description:** We are adding the ability to see current average code coverage for all the projects under a group with hisorical calculations that can be downloaded.
    - **Status:** Designed and currently in development (13.5). No known blockers.

<hr>

- #### Designs for Vision of Testing Categories
    - **Issue:** https://gitlab.com/gitlab-org/gitlab-design/-/issues/1217
    - **Description:** We are exploring our vision for the next three years in the Testing group. The goal of this initiative is to identify the foundational work for the next year and align short term goals with our long term vision.
    - **Status:** Designed wireframes exploring Dashboards, Actions in MRs. | _Cost Analysis not designed yet._
<hr>

- #### Test History MVC (In Report and MRs)
    - **Issue:** https://gitlab.com/gitlab-org/gitlab/-/issues/235525
    - **Issue(2):** https://gitlab.com/gitlab-org/gitlab/-/issues/241759
    - **Description:** We are adding the ability to see historical runs for each tests (in the test report and the summary widget) with the goal of helping customers to identify troubling tests.
    - **Status:** Validated and Designed. Awaiting for Further Validation or Final Implementation. 
<hr>

- #### Cleanup MR Language
    - **Issue:** https://gitlab.com/gitlab-org/gitlab/-/issues/214566 (Parent issue/ Other issue were spun off this one)
    - **Description:** We are working on standardizing the language of the Testing Merge Request Widgets so they are more precise and describe more strictly the type of result/summary that they are showing.
    - **Status:** Validated and Discussed. New issues created and awaiting for implementation.
<hr>

- #### Failed Tests in Screenshots and Screenshots Metadata
    - **Issue:** https://gitlab.com/gitlab-org/gitlab/-/issues/250692
    - **Description:** We now allow to take screenshots in tests through Selenium but we don't have any surface in UI to show those screenshots.  The goal here is to re-validate the orginal designs for this and refine them for implemetation. Previous designs: https://gitlab.com/gitlab-org/gitlab/-/issues/6061
    - **Status:** This issue has original designs, but this requires refinement for implementation.
<hr>

### Runner

- #### Runners Dashboard and Runner View Revamp
    - **Epic:**  https://gitlab.com/groups/gitlab-org/-/epics/4015
    - **Description:** We are revamping the runner admin/settings view across all levels so we can start solving some existing usability issues and create the foundation for the Runner Enterprise dashboard.
     - **Status:** Early design explorations done. Awaiting for further validation.
<hr>

- #### Disable Shared Runners at Group Level
    - **Issue:**  https://gitlab.com/gitlab-org/gitlab/-/issues/23123
    - **Description:** We are adding the ability to disable runners at the group level. This was originally developed by sub-contractors and have been in review/refactoring for a while. Expected to merge soon.
     - **Status:** Designed, Validated and expected to merge soon.
<hr>


- #### Runner Guided Install
    - **Issue:**  https://gitlab.com/gitlab-org/gitlab/-/issues/214587
    - **Description:** We are adding a new guided installation for runners from the Runner admin.
     - **Status:** Designed, Validated and expected to merge soon.
<hr>

- #### Standardize how we refer to runners and runner concepts
    - **Issue:**  https://gitlab.com/gitlab-org/gitlab/-/issues/233972
    - **Description:** Co-joint effort with Suzanne to improve the conceptual understanding of runners by changing vague terminology.
     - **Status:** Currently on discussions and brainstorming. 
<hr>

- #### Standardize capitalization of runner terminology
    - **Issue:**  https://gitlab.com/gitlab-org/gitlab/-/issues/214587
    - **Description:** We are standardizing the way we capitalize runner concepts (GitLab Runner vs runner)
     - **Status:** Blocked by the above terminology issue.
<hr>






### Pajamas


### GDK




## Direct Stakeholders

- **Nadia Udalova** (nudalova) | Product Design Manager, CI/CD | 
    - Stake Relation: `Direct Partner for Strategy/Tasks/Vision in ALL Initiaves and Manager`
    - Meetings: 
<br>

- **James Heimbuck** (jheimbuck) | Sr. Product Manager, Testing | 
    - Stake Relation: `Direct Partner for Strategy/Tasks/Vision in Testing`
    - Meetings: 
<br>

- **Darren Eastman** (deastman) | Sr. Product Manager, Runner | 
    - Stake Relation: `Direct Partner for Strategy/Tasks/Vision in Runner`
    - Meetings: 
<br>

- **Ricky Wiens** (rickywiens) | Engineering Manager, Testing | 
    - Stake Relation: `Direct Stakeholder for Engineering Related Strategy/Tasks in Testing`
    - Meetings: 
<br>

- **Elliot Rushton** (erushton) | Engineering Manager, Runner | 
    - Stake Relation: `Direct Stakeholder for Engineering Related Strategy/Tasks in Runner`
    - Meetings: 
<br>

- **Lorie Whitaker** (lwhitaker) | Staff UX Researcher | 
    - Stake Relation: `Direct Partner for Testing and Runner UX Research`
    - Meetings: 
<br>

- **Suzanne Selhorn** (sselhorn) | Sr. Technical Writer, Runner| 
    - Stake Relation: `Direct Partner for Runner Docs`
    - Meetings: 
<br>

- **Rayana Verissimo** (rverissimo) | Sr. Product Designer, Release Management | 
    - Stake Relation: `Direct Stakeholder for Pajamas Maintainer Training`
    - Meetings
<br>

- **Jeremy Elder** (jelder) | Sr. Product Designer, FE/UX Foundations |
    - Stake Relation: `UX Buddy`
    - Meetings: 




## Scope


## Dependencies


## 13.5 / 13.6 Horizon