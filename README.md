# README - Juan J. Ramirez, Sr. Product Designer Verify::Runner and Verify::Testing

## About
This is a project with information about me (Juan J. Ramirez, Sr. Product Designer for Verify::Runner and Verify::Testing). 
Here you can find information regarding my work and communication style, design process, ongoing OKRs and more.


## Index

- [😎  About Me](#about-me)
    - [Personal](#personal)
    - [Experience](#experience)

<br>

- [🤓  Work Style](#work-style)
    - [Work Tenants](#work-tenants)
    - [Design Process](#design-process)
    - [Communication](#communication)
    - [Schedule - Working Hours](#schedule-working-hours)


## About Me 

My name is Juan J. Ramirez. I have been a GitLabber since September 3rd, 2019. 
I'm a UX Designer who is passionate about building simple software for extremely complex problems.

### Personal
- 😄  Things I Enjoy:
    - Dogs (Especially my Dog, Moscu)
    - Walking-Jogging / Meditating
    - Art (Urban and Modern Art)
    - Traveling
    - Coffee and Tea 
    - Beer
    - Netflix Binge Watching
    - Buying Unnecessary Gadgets
    - Coding

<br>

- 😡  Things I Hate:
    - Fast Food Restaurants that don't serve their Food "Fast"

<br>

- 🗺  Where I'm From ~ Where I Lived:
    - Born in Medellin, Colombia 🇨🇴
    - Raised in Bogota, Colombia 🇨🇴
    - Lived in Boston, MA, USA 🇺🇸
    - Lived in Pittsburgh, PA, USA 🇺🇸
    - Lived in New York, NY, USA 🇺🇸
    - Lived in Southern California (SB, Ventura, LA Area), USA 🇺🇸
    - Lived in Seattle, WA, USA 🇺🇸
    - Currently living in Austin, TX, USA 🇺🇸


### Experience

- Years Designing Software: 
    - 9 Years

<br>

- Places Where I Worked Before: 
    - Amazon Web Services, Procore Technologies, Seconds.com and more.

<br>

- What Did I Study: 
    - HCI and Entertainment Tech Grad at Carnegie Mellon University  
    - Business Undergrad at CESA Business School

<hr>

## Work Style

My work style can be explained in a single sentence: 
I focus extremely hard in generating high quality output work that can help others to take decisions effectively and efficiently. 

### Work Tenants

- **Quality over Quantity:** Generating a lot of so so work will never be as good as generating the right amount of excellent work. Both will take the same amount of time but over optimizing for quantity only creates confusion (Imagine Michelangelo painting 1000 small paints instead of painting the Sixtine Chapel. I'm not saying that I'm Michelangelo but you get the idea). I focus on generating high quality/laser-focus work that becomes better through time with iteration and continuus analysis.

- **Flexibility:** I can produce a wide range of artifacts, from documents to code prototypes. I'm flexible and will always produce whatever artifact is needed to articulate a solution. I also can wear different hats and will always try to fill the gaps of broken processes if my ability and time allows it. I'll always share with others when this happens so we can improve the broken process.

- **Writing:** Writing is the best mechanism that I have to express and articulate ideas. You can call me via Zoom (please do) but you will probably get a clearer picture of my ideas on written mediums like Slack, Email and GitLab Comments. I find writing to be the perfect way to buffer ideas enough to see if they make sense. When I talk I don't have that buffer and my ideas may have more flaws and look more like rambles. So long-story short, if I give you an idea verbally wait for me to formalize it in writing. 

- **Creativity:** No good solution will come from uncreative thinking. Best solutions are generated through a creative process where we try to imagine a future that doesn't exist yet, instead of trying to imagine solutions that only fit our present state. I always try to imagine the best possible solution, even if it's not technically possible, and carve that idea into a more reasonable and attainable goal.

I probably have more work tenants and I'll likely add those in the future but for now these are good representation of my work style.


### Design Process

1) **Working Backward From the User/Customer:** I always start thinking about the final user and why they need some type of solution. The customers may not know exactly what they want, but they definitely know what they don't want. I try to understand their pespective and thoughts to create a solution that is based on their desires and pain-points.

2) **Passive Research:** Developers are opinionated. There are millions of thoughts and ideas about the Developer Tools space that have been captured somewhere in the internent. I always try to find this data to contrast it against my intuition and active research efforts. Some places where I look for this type of information are:
    - Hacker News
    - Stack Overflow
    - GitLab
    - GitHub
    - Developer Blogs

### Communication

- Besides the regularly schedule meetings, I personally use Zoom calls to quickly catch-up on existing initiatives and understand work and scope. I have found that Zoom calls are better for me to understand something than for someone else to understand my ideas. For that reason I prefer to express my ideas in writing, even if they don't involve a decision. See [Work Tenants](#work-tenants)

- Please feel free to Slack me at any time. In sake of Transparency. I will prefer if you do it through the relevant open channel so others can get visibility of what is being discussed, but feel free to Slack me directly if needed.

- I encourage you to tag me in any issue you think is of my interest or need my attention or input. I also encourage you to assign issues to me, when you have certainty that my input will be required eventually. 


### Schedule - Working Hours

I work from 8:30 AM to 4:30 PM [USA Central Time](https://time.is/Chicago) with some 1 hour earlier start (7:30 AM) / 2 hours later start (6:30 PM) for special events / meetings / request. If you need to schedule a meeting with me outside of my working hours, please contact me and we can figure it out.

I rarely change my work hours, but if I'm traveling (for instance, taking a flight), I may push my working hours a little bit. For those case I always try to update any relevant parties or systems with these changes.

