## Career Development (In Progress)

### Responsibilities 

- Deeply understand the technology and features of the stage group to which you are assigned, and have working knowledge of the end-to-end GitLab product.
  - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/3  

<hr>

- Proactively identify strategic UX needs within your stage group, and engage other UX Designers within your stage group to help you create strategic deliverables like Journey Maps, storyboards, competitive analyses, and personas.
  - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/4   

<hr>

- Proactively identify both small and large usability issues within your stage group, and help influence your Product Manager to prioritize them.
  - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/5  

<hr>

- Proactively identify user research needs, and conduct evaluative research with guidance from the UX Research team.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/6   

<hr>

- Broadly communicate the results of UX activities within your stage group to the UX department, cross-functional partners within your stage group, and other interested GitLabbers using clear language that simplifies complexity.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/7 

<hr>

- Mentor other members of the UX department, both inside and outside of your stage group.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/8   

<hr>

- Actively contribute to the Pajamas Design System.	
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/9   

<hr>

- Engage in social media efforts, including writing blog articles and responding on Twitter, as appropriate.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/10   

<hr>

- Interview potential UX candidates.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/11   


### Skills


- Does this person work well with others, do they work well with the manager? Do what is promised? Efficient and iterative? Inclusive?
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/12   

<hr>

- Is this person leading change? Are they innovative and a self-starter?
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/13   

<hr>

- Does this person continuously learn and grow with the support of GitLab and the manager?
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/14   

<hr>

- Are they acting as a mentor or leader in the team on assigned projects?
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/15  

<hr>

- Is this person's impact on their team and to the company positive or negative?
    - - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/16  


### Individual Growth Plan Goals

<hr>

- Coaching of others (including non-designers about design)
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/17  

<hr>

- Develop deep technical expertise about GitLab
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/18  

<hr>

- Deep Understanding of design tools / workflows and how to improve productivity through design workflows.
    - Tracking issue: https://gitlab.com/jj-ramirez/readme/issues/19  
